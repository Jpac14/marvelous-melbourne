import SectionContainer from "../components/SectionContainer"
import Helmet from "../components/Helmet"

import { SectionsContainer } from 'react-fullpage';

export default function Home() {
  let options = {
      sectionClassName: 'section',
      anchors: ['melbourne', 'botanic_gardens', 'polly_woodside', 'enterprize_park', 'queen_victoria_market', 'dfo', 'melbourne_star', 'mcg', 'south_bank_arts_centre', 'sandridge_bridge', 'culture_kings', 'federation_square', 'national_gallery', 'docklands', 'birrarung_marr', 'eureka_tower', 'yarra_river', 'credits'],
      scrollBar: false,
      navigation: true,
      arrowNavigation: true,
  }

  return (
    <div>
      <Helmet/>
      <SectionsContainer {...options}>
        <SectionContainer title="Visit Melbourne!" image="melbourne.jpeg">
          We&apos;re back in business
        </SectionContainer>
        <SectionContainer title="Botanic Gardens" image="botanic_gardens.jpeg">
          We&apos;re excited to welcome you back to Melbourne! The Botanic Gardens extends over 38 hectares and houses 
          a collection of more than 8,500 species of plants from around the world. The Gardens are a great place to 
          relax and find your inner self.
        </SectionContainer>
        <SectionContainer title="Polly Woodside" image="polly_woodside.jpeg">
          Polly Woodside is a Belfast-built, three-masted, iron-hulled barque, preserved in Melbourne and forming 
          the central feature of the South Wharf precinct. The ship was originally built in Belfast by William J. 
          Woodside and was launched in 1885. This is a great place to discover Melbourne&apos;s history.
        </SectionContainer>
        <SectionContainer title="Enterprize Park" image="enterprize_park.jpeg">
          Enterprize Park commemorates the spot where the first settlers aboard the ship Enterprize went 
          ashore on 30 August 1835. The Kulin people and their ancestors lived, loved and thrived on the banks of 
          the Birrarung. This ancient crossing point of the river, at a small waterfall, was a meeting place of 
          the Woiwurrung and Boonwurrong people. Enterprize park is a magnificent place to be yourself.
        </SectionContainer>
        <SectionContainer title="Queen Victoria Market" image="queen_victoria_market.jpg">
          The Queen Victoria Market was first opened on the 20th of March in 1878.
          From Apples to Zucchini the Queen Victoria Market has a variety of food, drink and clothing. The Queen Victoria also has souvenirs and toys. Overall the market is a great place to go to in Melbourne.
        </SectionContainer>
        <SectionContainer title="DFO South Wharf" image="dfo.jpg">
          DFO is a shopping centre in Melbourne. It was first opened in 1992. It is a great place to buy shoes, clothing, food, drink and other great items. at DFO you can find stores like Nike, Adidas, Tommy Hilfinger and Converse. These are only some of the stores.
        </SectionContainer>
        <SectionContainer title="Melbourne Star" image="melbourne_star.jpg">
          This stunning ferris wheel is only one of the Spectacular attractions in Melbourne. The Melbourne Star is 120 metres tall. It is best to see this attraction at night time as the bright colours will make this ferris wheel stand out. The Melbourne Star took 7 years to finish building. It started in 2006 and finished in 2013.
        </SectionContainer>
        <SectionContainer title="MCG" image="mcg.jpg">
          The MCG is a sports stadium that is used for many things, especially it is used for the home and away season of the AFL, which is a big part in the history of the AFL, finals and the grand final are traditionally played here every year, I recommend in attending a game at the MCG because the atmosphere is something you will never forget.
        </SectionContainer>
        <SectionContainer title="South Bank Arts Centre" image="south_bank_arts_centre.jpg">
          Arts Centre Melbourne was originally known as the Victorian Arts Centre and it is a performing arts centre consisting of a complex of theatres and concert halls. If you&apos;re into concerts and people performing I would recommend visiting this place in the heart of melbourne.
        </SectionContainer>
        <SectionContainer title="Sandridge Bridge" image="sandridge_bridge.jpg">
          Sandridge Railway Bridge is a very interesting bridge which used to be a railway bridge but has now been transformed into a pedestrian and cycle pathway over the Yarra River. It is particularly worthwhile visiting because it is a museum of the history of immigration to Melbourne and Australia.
        </SectionContainer>
        <SectionContainer title="Culture Kings" image="culture_kings.jpg">
          Culture Kings is a streetwear company with the best of the best clothing your favourite superstars shop here like Lil Yachty, Chris Brown, Future, Drake and many more. Culture Kings have over 100 brands and 3 thousand clothing items in store, so you won&apos;t miss out on your favourite item.
        </SectionContainer>
        <SectionContainer title="Federation Square" image="federation_square.jpg">
          Federation Square is a venue for arts, culture and public events on the edge of the Melbourne central business district. Moments after departing Melbourne&apos;s Cultural Arts precinct you are cruising through Melbourne&apos;s picturesque gardens, parkland&apos;s and some of our famous sporting arenas. It&apos;s a great place to enjoy yourself.
        </SectionContainer>
        <SectionContainer title="National Gallery of Victoria" image="national_gallery.jpg">
          The National Gallery of Victoria, popularly known as the NGV, is an art museum in Melbourne, Victoria, Australia. It&apos;s a great place to go with the family and enjoy the beautiful art. NGV has many types of art from paintings to statues to waterfalls the gallery has a variety of artwork.
        </SectionContainer>
        <SectionContainer title="Docklands" image="docklands.jpg">
          The suburb&apos;s 200 hectares of land and water are on Victoria Harbour the west of the city centre the Docklands is in the City of Melbourne doubled the size of Melbourne&apos;s central city and returned a significant area of waterfront to the city. The docklands used to be a gas works and railway industry but now is gathering populations of people to come and visit dining and cultural activities. 
        </SectionContainer>
        <SectionContainer title="Birrarung Marr" image="birrarung_marr.jpg">
          Birrarung Marr is an urban park on the North side of the river. The winding path acknowledges what traditional food sources aboriginals used to eat. It is a great sight to see and 100% worth your time.
        </SectionContainer>
        <SectionContainer title="Eureka Tower" image="eureka_tower.jpg">
          Eureka Tower is a 297.3 m skyscraper located in the Southbank precinct of Melbourne, Victoria, Australia. It&apos;s a great place to go with the family and go sightseeing and see the beautiful view of Melbourne. From the top of the tower you can see all the other buildings. It is best to see it at night as that is when the lights turn on and the city is alive.
        </SectionContainer>
        <SectionContainer title="Yarra River" image="yarra_river.jpg">
          The name Yarra is attributed to surveyor John Wedge, Birrarung is the Wurundjeri name for the waterway, but Yarra is also an indigenous word meaning waterfall or ever flowing. The river is believed to have got its current name after an Englishman mistakenly believed Yarra Yarra was its Aboriginal name. The Wurundjeri people the river, known as Birrarung, was their life and an important meeting place. And, to many locals, where happy memories of personal pilgrimages, peaceful summer evenings, and days exploring were made. The Yarra River or historically is a perennial river in south central Victoria, Australia and the Yarra River is a good tourist attraction and it is used for transport.
        </SectionContainer>
        <SectionContainer title="By" image="friends.jpeg">
          Joshua, Milan, Abu, Jordan
        </SectionContainer>
      </SectionsContainer>
    </div>
  )
}
