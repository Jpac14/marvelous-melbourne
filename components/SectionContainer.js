import { Section } from 'react-fullpage';

export default function SectionContainer({ title, children, image }) {
	return (
		<Section>
			<div className="bg">
				<div className="text">
					<h1>{title}</h1>
					<p>{children}</p>
				</div>

				<div className="footer">
					Made with <span className="red">&#10084;</span> by <a className="link" target="_blank" rel="noreferrer" href="https://gitlab.com/Jpac14">Jpac14</a>
				</div>

			
				<style jsx>{`
					.bg {
						background-image: url(${image});
						background-repeat: no-repeat;
						background-size: cover;
						background-position: center;
						height: 100%;
						display: flex;
						flex-direction: column;
						align-items: center;
						justify-content: center;
						height: 100%;
						position: relative;
					}
			
					.text {
						text-align: center;
						background-color: rgba(0, 0, 0, 0.7);
						border-radius: 2em;
						margin: 4em;
						padding: 1em;
						color: white;
					}
			
					.text h1 {
						font-size: 5em;
					}
					.text p {
						font-size: 2em;
					}
			
					h1, p {
						color: white;
						margin: 0;
					}

					.footer {
						position: absolute;
						bottom: 0;
        		left: 0;
					  background-color: rgba(0, 0, 0, 0.7);
					  border-radius: 2em;
					  padding: 0.5em;
					  color: white;
					  margin-left: 0.5em;
					  margin-bottom: 0.5em;
					}

					.red {
						color: red;
					}

					.link {
						text-decoration: underline;
					}

					@media screen and (max-width: 600px) {
						.text h1 {
							font-size: 3em;
						}
						.text p {
							font-size: 1.2em;
						}
					}
				`}</style>
			</div>
		</Section>
	)
}
