import Head from 'next/head'

export default function Helmet() {
	return (
		<Head>
			<title>Marvelous Melbourne</title>
		</Head>
	)
}