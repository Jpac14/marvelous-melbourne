# Contributing
If you would like to contribute, either look for a issue to fix or create a issue if you have one. Once completed you can create a merge request and then start working on the issue inside that branch. When completed, you can push your changes and then mark the merge request as complete, soon someone will look over your merge request and check it is satisfactory.
